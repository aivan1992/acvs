import Axios from "axios";
import { default_header, multipart_header } from "@/requestHeaders.js";

const sendMessage = (state, payload) => {
  return new Promise((resolve, reject) => {
    Axios.post("activity/send-message", payload, default_header)
      .then(res => {
        if (res.status == 200) {
          resolve(true);
        }
      })
      .catch(err => {
        reject(err);
      });
  });
};

const getTransactions = (state, payload) => {
  return new Promise((resolve, reject) => {
    Axios.post("activity/transactions", payload, default_header)
      .then(res => {
        if (res.status == 200) {
          resolve(res.data);
        }
        reject('invalid');
      })
      .catch(err => {
        reject(err);
      });
  });
};


export default {
  sendMessage,
  getTransactions
};
