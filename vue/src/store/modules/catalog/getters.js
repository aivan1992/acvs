const getNewArrivals = state => {
  return state.newArrivals;
};

const getAllCatalogs = state => {
  return state.allCatalogs.data;
};

const getAllCatalogsPagination = state => {
  return state.allCatalogs.pagination;
};

const getCategories = state => {
  return state.categories;
};

const getSingleCatalog = state => {
  return state.singleCatalog;
};

const searchCatalog = state => {
  return state.searchCatalog;
};

export default {
  getNewArrivals,
  getAllCatalogsPagination,
  getAllCatalogs,
  getCategories,
  getSingleCatalog,
  searchCatalog
};
