import actions from "./actions";
import getters from "./getters";
import mutations from "./mutations";

const state = {
  allCatalogs: {
    data: null,
    pagination: null
  },
  newArrivals: null,
  categories: null,
  singleCatalog: null,

  searchCatalog: null
};

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations
};
