import Axios from "axios";
import { default_header, multipart_header } from "@/requestHeaders.js";

const getAllCatalogs = ({ commit }, payload) => {
  /**
   *  @params payload.data, payload.nextPrev
   *
   *  Set empty string nextPrev if undefined otherwise append the nextPrev to url
   *
   */

  let nextPrev = payload.nextPrev != undefined ? payload.nextPrev : "";
  /**/

  /**
   * Convert JSON to URL query string
   *
   */
  var queryString = Object.keys(payload.data)
    .map(key => {
      return (
        encodeURIComponent(key) + "=" + encodeURIComponent(payload.data[key])
      );
    })
    .join("&");
  /**/

  return new Promise((resolve, reject) => {
    Axios.get(`catalogs?${queryString} ${nextPrev}`, default_header)
      .then(res => {
        if (res.status == 200) {
          console.log(res.data);
          commit("setAllCatalogs", res.data);
          resolve(true);
        }
      })
      .catch(err => {
        reject(err);
      });
  });
};

const getCategories = ({ commit }, payload) => {
  return new Promise((resolve, reject) => {
    Axios.get("catalogs/categories", default_header)
      .then(res => {
        if (res.status == 200) {
          commit("setCategories", res);
          resolve(true);
        }
      })
      .catch(err => {
        reject(err);
      });
  });
};

const getSingleCatalog = ({ commit }, payload) => {
  return new Promise((resolve, reject) => {
    Axios.get(`catalogs/${payload.id}`, default_header)
      .then(res => {
        if (res.status == 200) {
          commit("setSingleCatalog", res.data);
          resolve(true);
        }
      })
      .catch(err => {
        reject(err);
      });
  });
};

const submitInquire = ({ commit }, payload) => {
  return new Promise((resolve, reject) => {
    Axios.post("catalogs/submit-inquire", payload, default_header)
      .then(res => {
        if (res.status == 200) {
          resolve(true);
        }
      })
      .catch(err => {
        reject(err);
      });
  });
};

export default {
  submitInquire,
  getAllCatalogs,
  getCategories,
  getSingleCatalog
};
