const setNewArrivals = (state, payload) => {
  state.newArrivals = payload.data;
};

const setAllCatalogs = (state, payload) => {
  state.allCatalogs.data = payload.data;
  state.allCatalogs.pagination = payload.pagination;
};

const setCategories = (state, payload) => {
  state.categories = payload.data;
};

const setSingleCatalog = (state, payload) => {
  state.singleCatalog = payload;
};

const setSearchCatalog = (state, payload) => {
  state.searchCatalog = payload;
};

export default {
  setNewArrivals,
  setAllCatalogs,
  setCategories,
  setSingleCatalog,
  setSearchCatalog
};
