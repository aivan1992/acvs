const setAuth = state => {
  state.Authentication = "Bearer " + localStorage.getItem("token");
};

const setEmail = (state, payload) => {
  state.email = payload;
};

const setPassword = (state, payload) => {
  state.password = payload;
};
const setName = (state, payload) => {
  state.name = payload;
};
export default {
  setAuth,
  setEmail,
  setPassword,
  setName
};
