import Axios from "axios";

const loginUser = ({ commit, state }, payload) => {
  return new Promise((resolve, reject) => {
    Axios.post("login", {
      email: state.email,
      password: state.password
    })
      .then(response => {
        if (response.status === 200) {
          state.Authentication = response.data.success.token;
          localStorage.setItem("token", state.Authentication);
          commit("setAuth");
          resolve(true);
        }
      })
      .catch(err => {
        reject(err);
      });
  });
};

const logoutUser = state => {
  return new Promise((resolve, reject) => {
    localStorage.removeItem("token");
  });
};

const registerUser = (state, payload) => {
  return new Promise((resolve, reject) => {
    Axios.post("register", {
      email: state.state.email,
      password: state.state.password,
      name: state.state.name
    })
      .then(response => {
        console.log(response);
        if (response.status == "201") {
          resolve(true);
        }
      })
      .catch(err => {
        reject(err);
      });
  });
};

export default {
  loginUser,
  registerUser,
  logoutUser
};
