import Vue from "vue";

import Vuex from "vuex";
Vue.use(Vuex);
import "./axiosInterceptors";

import auth from "@/modules/auth/store";
import activity from "./modules/activity";
import account from "./modules/account";
import catalogs from "./modules/catalog";

const store = new Vuex.Store({
  modules: {
    auth,
    activity,
    account,
    catalogs
  },
  state: {
    displayAuth: false,
    showPreloader: false
  },
  mutations: {
    displayAuth(state, payload) {
      state.displayAuth = payload;
    },
    showPreloader(state, payload) {
      state.showPreloader = payload;
    }
  },
  getters: {
    displayAuth(state) {
      return state.displayAuth;
    },
    showPreloader(state, payload) {
      return state.showPreloader;
    }
  }
});

export default store;
