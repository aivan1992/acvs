
import Vue from "vue";
import { BootstrapVue } from "bootstrap-vue";
import "@/externalscript";
import App from "./App.vue";
import router from "./router";

import Axios from "axios";
Axios.defaults.baseURL = "http://app-animal-clinic-volunteer-system.ivanrobertlaid.ml/api";

import store from "./store";

import customPlugin from "./custom-plugins";
Vue.use(customPlugin);

import countUp from "vue-countup-directive";
Vue.directive("countUp", countUp);

import VueFbCustomerChat from "vue-fb-customer-chat";
Vue.use(VueFbCustomerChat, {
  page_id: 109473373838403,
  theme_color: "#F48A3D", // theme color in HEX
  locale: "en_US", // default 'en_US'
});
Vue.use(BootstrapVue);

/*
 *  IMPORT THE GLOBAL MIXIN HELPERS
 */
import { mixin } from "@/mixins/index";
/*
 */

import * as VueGoogleMaps from "vue2-google-maps";

Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyBqDaBjJkdEGkVZGl2czayjsWN2eCsBMx0",
    libraries: "places",
  },
});

import Swal from "sweetalert2";
window.Swal = Swal;

router.beforeEach((to, from, next) => {
  // to and from are both route objects. must call `next`.

  if (to.meta.noAuth) {
    if (localStorage.getItem("token")) {
      store.commit("displayAuth", false);
      next();
    } else {
      next();
    }
  } else {
    if (localStorage.getItem("token")) {
      store.commit("displayAuth", false);
      next();
    } else {
      store.commit("displayAuth", true);
      next({ name: "Home" });
    }
  }
});

router.beforeEach((to, from, next) => {
  // This goes through the matched routes from last to first, finding the closest route with a title.
  // eg. if we have /some/deep/nested/route and /some, /deep, and /nested have titles, nested's will be chosen.
  const nearestWithTitle = to.matched
    .slice()
    .reverse()
    .find((r) => r.meta && r.meta.title);

  // Find the nearest route element with meta tags.
  const nearestWithMeta = to.matched
    .slice()
    .reverse()
    .find((r) => r.meta && r.meta.metaTags);
  const previousNearestWithMeta = from.matched
    .slice()
    .reverse()
    .find((r) => r.meta && r.meta.metaTags);

  // If a route with a title was found, set the document (page) title to that value.
  if (nearestWithTitle) document.title = nearestWithTitle.meta.title;

  // Remove any stale meta tags from the document using the key attribute we set below.
  Array.from(document.querySelectorAll("[data-vue-router-controlled]")).map(
    (el) => el.parentNode.removeChild(el)
  );

  // Skip rendering meta tags if there are none.
  if (!nearestWithMeta) return next();

  // Turn the meta tag definitions into actual elements in the head.
  nearestWithMeta.meta.metaTags
    .map((tagDef) => {
      const tag = document.createElement("meta");

      Object.keys(tagDef).forEach((key) => {
        tag.setAttribute(key, tagDef[key]);
      });

      // We use this to track which meta tags we create, so we don't interfere with other ones.
      tag.setAttribute("data-vue-router-controlled", "");

      return tag;
    })
    // Add the meta tags to the document head.
    .forEach((tag) => document.head.appendChild(tag));

  next();
});

Vue.directive("click-outside", {
  bind: function(el, binding, vnode) {
    window.event = function(event) {
      if (!(el == event.target || el.contains(event.target))) {
        vnode.context[binding.expression](event);
      }
    };
    document.body.addEventListener("click", window.event);
  },
  unbind: function(el) {
    document.body.removeEventListener("click", window.event);
  },
});

Vue.config.productionTip = false;

var app = new Vue({
  mixins: [mixin],
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");

global.vm = app;
