import Vue from "vue";
import Router from "vue-router";
Vue.use(Router);
export default new Router({
  mode: "history",
  // base: process.env.BASE_URL,
  scrollBehavior: () => ({
    y: 0
  }),
  routes: [
    {
      path: "/",
      name: "Home",
      component: () => import("../views/Home.vue"),
      meta: {
        title: "Home - Fauna Care",
        noAuth: true,
        metaTags: [
          {
            name: "description",
            content: "Home page of example"
          },
          {
            property: "og:description",
            content: "Home page of example"
          }
        ]
      }
    },
    {
      path: "/contact",
      name: "Contact",
      component: () => import("../views/pages/Contact.vue"),
      meta: {
        title: "Contact - Fauna Care"
      }
    },
    {
      path: "/testimonials",
      name: "Testimonials",
      component: () => import("../views/pages/Testimonials.vue"),
      meta: {
        title: "Testimonials - Fauna Care"
      }
    },

     {
      path: "/transactions",
      name: "Transactions",
      component: () => import("../views/pages/Transactions.vue"),
      meta: {
        title: "Transactions - Fauna Care"
      }
    },
    {
      path: "/my-account",
      name: "My account",
      component: () => import("../views/pages/MyAccount.vue"),
      meta: {
        title: "My Account - Fauna Care"
      }
    },
    {
      path: "/services",
      name: "Services",
      component: () => import("../views/pages/Services.vue"),
      meta: {
        title: "Services - Fauna Care"
      }
    },
      {
      path: "/catalog/inquire/:id",
      name: "Inquire",
      component: () => import("../views/pages/Inquire.vue"),
      meta: {
      title: "Inquire - Fauna Care"
      }
    },
    {
      path: "/catalog",
      name: "Catalog",
      component: () => import("../views/pages/Catalog.vue"),
        meta: {
      title: "Catalog - Fauna Care"
      }
    },
    {
      path: "/about",
      name: "About",
      component: () => import("../views/pages/About.vue"),
      meta: {
        title: "About - Fauna Care"
      }
    },
    {
      path: "/",
      component: {
        render(c) {
          return c("router-view");
        }
      },
      children: [
        {
          path: "/auth",
          name: "Auth",
          component: () => import("@/views/pages/Auth.vue"),
          meta: {
            noAuth: true,
            title: `Auth - Mom's Family Clinic`
          }
        },
        {
          path: "*",
          redirect: "/home"
        }
      ]
    }
  ]
});
