const getAuth = state => {
  return "Bearer " + localStorage.getItem("token");
};
const getEmail = state => {
  return state.email;
};
const getPassword = state => {
  return state.password;
};

export default {
  getAuth,
  getEmail,
  getPassword
};
