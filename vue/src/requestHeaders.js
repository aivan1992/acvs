export const default_header = {
  headers: {
    Authorization: "Bearer " + localStorage.getItem("token")
  }
};
export const multipart_header = {
  headers: {
    // 'Content-Type':"multipart/form-data; charset=utf-8; boundary=" + Math.random().toString().substr(2),
    "Content-Type": "multipart/form-data",
    Authorization: "Bearer " + localStorage.getItem("token")
  }
};
