<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('/', 'HomeController@index');

// Route::group(['middleware' => ['auth', 'role:super-admin,sub-admin']], function () {


Route::group(['namespace' => 'AdminPanel'], function () {

Route::post('/accounts/search', 'AccountsResource@search');
Route::resource('/accounts', 'AccountsResource');

Route::get('/visits/escalated', 'VisitControllerResource@getEscalated');
Route::post('/visits/status', 'VisitControllerResource@updateStatus')->name('visits.status');
Route::resource('/visits', 'VisitControllerResource');

Route::post('/catalogs/status', 'CatalogControllerResource@updateStatus')->name('catalogs.status');

Route::post('/categories/search', 'CategoriesControllerResource@search');
Route::resource('/categories', 'CategoriesControllerResource');

Route::post('/breeds/search', 'BreedControllerResource@search');
Route::resource('/breeds', 'BreedControllerResource');

Route::resource('/catalogs', 'CatalogControllerResource');
Route::resource('/contact-details', 'ContactDetailControllerResource');



});

Route::get('/{page}', 'HomeController@page');

// });



