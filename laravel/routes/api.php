<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/




Route::group(['namespace' => 'Api'], function () {

	Route::post('login', 'AuthController@login');

	Route::post('register', 'AuthController@register');


	Route::group(['middleware' => 'auth:api'], function () {

		Route::post('catalogs/submit-inquire', 'CatalogController@submitInquire');
		Route::get('catalogs/categories', 'CatalogController@categories');
		Route::resource('catalogs', 'CatalogController');
		Route::resource('account', 'AccountController');
		Route::post('activity/send-message', 'ActivityController@sendMessage');
		Route::post('activity/transactions', 'ActivityController@getTransactions');
	});
});