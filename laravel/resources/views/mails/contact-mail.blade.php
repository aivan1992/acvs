@component('mail::layout')

@slot('header')
@component('mail::header',['url' => ''])
<span class="bg-danger">CARE ANIMAL SHELTER</span> 
@endcomponent
@endslot

#Hello, greetings admin<br>
A client contacted you for inquiry. the message information is listed below.
<br>

#Name: {{$info['name']}}
#Subject: {{$info['subject']}}
#Email: {{$info['email']}}
#Message: {{$info['message']}}


@slot('footer')
@component('mail::footer')
Copyright {{ now()->year }} CARE ANIMAL SHELTER
@endcomponent
@endslot

@endcomponent
