@component('mail::layout')

@slot('header')
@component('mail::header',['url' => ''])
<span class="bg-danger">CARE ANIMAL SHELTER</span> 
@endcomponent
@endslot

#Hello, {{$name}}<br>
This is to inform you that the admin received your inquiry. Thanks for contacting us.

@slot('footer')
@component('mail::footer')
Copyright {{ now()->year }} CARE ANIMAL SHELTER
@endcomponent
@endslot

@endcomponent
