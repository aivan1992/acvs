  @extends('template.main')

  @section('body')

  @include('partials.card-header', ['title' => 'Catalog Lists'])

  @include('partials.success-error-notification')
  
  @include('partials.search-form', [ 'route' => '/catalogs/search', 
  'searchKey' => [
  ['value' => 'serial', 'text' => 'SERIAL'],
  ['value' => 'id', 'text' => 'CATALOG ID'],
  ['value' => 'title', 'text' => 'CATALOG TITLE']
  ]])


  <div class="col-lg-12 grid-margin stretch-card">
   <div class="card">
    <div class="card-body">
     <table class="table table-hover">
      <thead>
       <tr>
        <th>CATALOG ID<a href="Javascript:void(0)" title="Click serial to view image"><i class="fa fa-camera text-primary"></i></a></th>
        <th>CATALOG NAME</th>
        <th>BREED</th>
        <th>CATEGORY</th>
        <th>STATUS</th>
        <th>ACTIONS</th>
        <th>REMOVE CATALOG</th>
      </tr>
    </thead>
    <tbody>

      @foreach($catalogs as $catalog)

      <tr>
        <td><a href="{{route('catalogs.show', $catalog->id)}}" target="_blank" class="custom-link-text">{{$catalog->id}}</a></td>
        <td>
          <textarea readonly class="form-control">{{$catalog->name}}</textarea>
        </td>
       
        <td>
          {{$catalog->breed->value('name')}}
        </td>
        <td>
          {{$catalog->category->value('name')}}
        </td>
        <td>

         <label class="badge {{$catalog->status ? 'badge-success' : 'badge-danger'}} custom-min-width-label">{{$catalog->status ? 'available' : 'unavailable'}}</label>

       <!--<a href="Javascript:void(0)" title="{{$catalog->status ? 'available' : 'unavailable'}}">
         <i class="fa {{ $catalog->status ? 'fa-ban text-success' : 'fa-check text-danger'}} "></i></a> -->
       </td>

       <td>
        <ul class="list-group custom-lists-horizontal">
          <li > <a title="edit catalog" href="{{route('catalogs.edit', $catalog->id)}}" class="btn social-btn btn-inverse-secondary">
            <i class="fa fa-edit text-black"></i>
          </a>
        </li>
        <li>
          <button type="type" data-toggle="tooltip" title="mark as available" onclick="updateStatus(1, '{{$catalog->id}}')" class="btn social-btn btn-inverse-secondary">
            <i class="fa fa-check text-black"></i>
          </li>
          <li>
           <a title="mark as unavailable" onclick="updateStatus(0 ,'{{$catalog->id}}')" class="btn social-btn btn-inverse-secondary">
            <i class="text-black fa fa-ban"></i>
          </a>
        </li>
      </ul>

    </td>
    <td>

      <a onclick="destroyCatalog('{{$catalog->id}}')" title="remove catalog" class="btn social-btn btn-inverse-secondary">
        <i class="fa fa-trash-o text-black"></i>
      </a>
    </td>
  </tr>

  @endforeach

</tbody>
<tfoot>
  <tr class="custom-pagination-space-around">
    <td><a href="/catalogs/create" class="btn btn-primary btn-fw">Add Catalog</a></td>
    <td colspan="5">{{$catalogs->links()}}</td> 
  </tr>
</tfoot>
</table>

</div>
</div>
</div>



<script>



  const updateStatus = (value, id) =>{

    if(confirm('Are you sure you want to proceed')) {
     $.ajaxSetup({
      headers: { 'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content }
    });

     $.ajax({
      url: "{{url('/catalogs/status')}}",
      method: 'POST',
      data: { 
       status : value,
       id
     },
     success:  res => {
      alert(JSON.stringify(res.success))
      window.location.reload();
    },  
    catch : err => {
     alert(JSON.stringify(err))
   }})
   }

 }
 const destroyCatalog = id => {

  if(confirm('Are you sure you want to proceed')) {
   $.ajaxSetup({
    headers: { 'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content }
  })
   $.ajax({
    url: '/catalogs/'+id,
    method: 'DELETE',
    data: { 
     id
   },
   success:  res => {
    alert(JSON.stringify(res.success))
    window.location.reload();
  },  
  catch : err => {
   alert(JSON.stringify(err))
 }})
 }

} 


</script>

@endsection