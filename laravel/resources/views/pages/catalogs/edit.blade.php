@extends('template.main')

@section('body')

@include('partials.card-header', ['title' => 'Edit Catalog'])

@include('partials.success-error-notification')

<div class="row">
	<div class="col-lg-12 d-flex justify-center ">
		<div class="col-lg-6 grid-margin stretch-card offset-lg-3">
			<div class="card">
				<div class="card-body">

					<form class="forms-sample"  action="{{route('catalogs.update', $catalog->id)}}" method="POST">
						@method('PUT')
						@csrf


						<div class="form-group">
							<label>Catalog ID</label>
							<input type="text" readonly  value="{{$catalog->id}}" class="form-control" >
						</div>


						<div class="form-group">
							<label >name</label>
							<input type="text" value="{{$catalog->name}}" name="catalog[name]" class="form-control" >
						</div>

						<div class="form-group">
							<label>Status</label>
							<select class="form-control" name="catalog[status]">
								@if($catalog->status)
								<option value="1" selected>Avialable</option>
								@else
								<option value="0">Not available</option>
								@endif
							</select>
						</div>


						<div class="form-group">
							<label>Breed</label>
							<select class="form-control" name="catalog[breed_id]" >
								@foreach($breeds as $breed)
								@if($catalog->breed()->value('id') == $breed->id)
								<option value="{{$breed->id}}" selected>{{$breed->name}}</option>
								@else
								<option value="{{$breed->id}}">{{$breed->name}}</option>
								@endif

								@endforeach
							</select>
						</div>


						<div class="form-group">
							<label>Category</label>
							<select class="form-control" name="catalog[category_id]" >
								@foreach($categories as $category)

								@if($catalog->category()->value('id') == $category->id)
								<option value="{{$category->id}}" selected>{{$category->name}}</option>
								@else
								<option value="{{$category->id}}">{{$category->name}}</option>
								@endif

								@endforeach
							</select>
						</div>
						<div class="form-group">
							<label>Image upload</label>
							<div class="input-group">
								<span class="input-group-btn bg-secondary">
									<span class="btn btn-default btn-file">
										Browse… <input type="file" name="image" id="imgInp">
									</span>
								</span>
								<input type="text"  class="form-control bg-transparent"  readonly>
							</div>
							@if($catalog->image_path)
							<img id='img-upload' src="{{$catalog->image_path}}" />
							@else
							<img id='img-upload'/>
							@endif
						</div>

						<div class="mt-3">
							<button type="submit" class="btn btn-primary mr-2">Submit</button>
							<a href="{{route('catalogs.index')}}" class="btn btn-light">Cancel</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection