	@extends('template.main')

@section('body')

@include('partials.card-header', ['title' => 'Add Catalog'])

@include('partials.success-error-notification')

<div class="row">
	<div class="col-lg-12 d-flex justify-center ">
		<div class="col-lg-6 grid-margin stretch-card offset-lg-3">
			<div class="card">
				<div class="card-body">

					<form class="forms-sample"  action="{{route('catalogs.store')}}" method="POST" enctype="multipart/form-data" >
						@csrf
						<div class="form-group">
							<label >name</label>
							<input type="text" name="catalog[name]" class="form-control" >
						</div>

						<div class="form-group">
							<label>Status</label>
							<select class="form-control" name="catalog[status]">
								<option value="1" >Available</option>
								<option value="0" selected>Not available</option>
							</select>
						</div>

					<div class="form-group">
							<label>Breed</label>
							<select class="form-control" name="catalog[breed_id]">
								@foreach($breeds as $breed)
								<option value="{{$breed->id}}">{{$breed->name}}</option>
								@endforeach
							</select>
						</div>

						<div class="form-group">
							<label>Category</label>
							<select class="form-control" name="catalog[category_id]">
								@foreach($categories as $category)
								<option value="{{$category->id}}">{{$category->name}}</option>
								@endforeach
							</select>
						</div>



						<div class="form-group">
							<label>Image upload</label>
							<div class="input-group">
								<span class="input-group-btn bg-secondary">
									<span class="btn btn-default btn-file">
										Browse… <input type="file" name="image" id="imgInp">
									</span>
								</span>
								<input type="text"  class="form-control bg-transparent"  readonly>
							</div>
							<img id='img-upload'/>
						</div>

						<div class="mt-3">
						<button type="submit" class="btn btn-primary mr-2">Submit</button>
						<a href="{{route('catalogs.index')}}" class="btn btn-light">Cancel</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection