@extends('template.main')


@section('body')

 @include('partials.card-header', ['title' => 'View catalog'])

@include('partials.success-error-notification')

<div class="row">
	<div class="col-lg-12 d-flex justify-center ">
		<div class="col-lg-6 grid-margin stretch-card offset-lg-3">
			<div class="card">
				<div class="card-body">



						<div class="form-group">
							<label >catalog ID</label>
							<input type="text" readonly  value="{{$catalog->id}}" class="form-control" >
						</div>
							<div class="form-group">
							<label >name</label>
							<input type="text" name="catalog[name]" class="form-control" >
						</div>


						<div class="form-group">
							<label>Status</label>
							<select class="form-control" name="catalog[status]">
								@if($catalog->status)
								<option value="1" selected>Avialable</option>
								@else
								<option value="0">Not available</option>
								@endif
							</select>
						</div>


	<div class="form-group">
							<label>Breed</label>
							<select class="form-control" name="catalog[breed_id]" >
								@foreach($breeds as $breed)
								@if($catalog->breed()->value('id') == $breed->id)
								<option value="{{$breed->id}}" selected>{{$breed->name}}</option>
								@else
								<option value="{{$breed->id}}">{{$breed->name}}</option>
								@endif

								@endforeach
							</select>
						</div>

						<div class="form-group">
							<label>catalog Category</label>
							<select class="form-control" name="catalog[category_id]" disabled  >
								@foreach($categories as $category)

								@if($catalog->category()->value('id') == $category->id)
								<option value="{{$category->id}}" selected>{{$category->name}}</option>
								@else
								<option value="{{$category->id}}">{{$category->name}}</option>
								@endif

								@endforeach
							</select>
						</div>

						<div class="form-group">
							<label>Image</label>
							<img id='img-upload' src="{{$catalog->image_path}}" />
						</div>


						<!-- <div class="form-group">
							<label>Notes</label>
							<textarea readonly class="form-control" name="catalog[notes]" rows="2">{{$catalog->notes}}</textarea>
						</div> -->
				</div>
			</div>
		</div>
	</div>
</div>
@endsection