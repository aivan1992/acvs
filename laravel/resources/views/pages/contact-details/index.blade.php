

@extends('template.main')

@section('body')

@include('partials.card-header', ['title' => 'CONTACT DETAILS'])

@include('partials.success-error-notification')


<div class="col-lg-4 mx-auto">
 <div class="card p-3">
  <form method="POST" action="{{route('contact-details.store')}}">
      @csrf
   <div class="form-group"> 
    <label>Address</label>
    <textarea name="address"  value="{{$contactDetails['address']}}" class="form-control" rows="10" required></textarea>
  </div>

  <div class="form-group"> 
    <label>Email</label>
    <input type="text" class="form-control" name="email" placeholder="admin@example.com" required />
  </div> 

  <div class="form-group"> 
    <label>Office sched</label>
    <input type="text" class="form-control" name="office_sched" placeholder="Mon to Fri 9am to 6 pm" required />
  </div> 

  <div class="form-group"> 
    <label>Contact Number</label>
    <input type="text" class="form-control" name="number" placeholder="+639 272332292" required />
  </div> 
  
  <div class="mt-5 pull-right">
    <a href="{{route('contact-details.index')}}" class="btn btn-light">Cancel</a>
    <button type="submit" class="btn btn-primary mr-2">Submit</button>
  </div>
</form> 
</div>
</div>


@endsection