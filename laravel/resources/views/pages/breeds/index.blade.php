@extends('template.main')

@section('body')

@include('partials.card-header', ['title' => 'BREED LISTS'])

@include('partials.success-error-notification')

<div class="col-lg-7 mx-auto">
  <form method="POST" action="breeds/search" id="search">
    @csrf
    <div class="form-group">
      <div class="input-group">
       <input type="text" class="form-control" name="search" style="font-size: 1.2em" placeholder="Search" aria-label="Search" aria-describedby="colored-addon3">
       <div class="input-group-append bg-secondary border-primary">
        <span class="input-group-text bg-transparent">
         <a style="cursor: pointer;" onclick="document.querySelector('#search').submit()">
          <i class="fa fa-search "></i>
        </a>
      </span>
    </div>
  </div>
</div>
</form>
</div>

<div class="col-lg-7 mx-auto grid-margin stretch-card">
 <div class="card">
  <div class="card-body">
    <div class="table-responsive">
     <table class="table table-hover">
      <thead>
       <tr>
        <th>BREED NAME</th>
        <th>EDIT</th>
        <th>REMOVE</th>
      </tr>
    </thead>
    <tbody>
      @foreach($breeds as $breed)
      <tr>
        <td>{{$breed->name}}</td>
        <td> <a  href="{{route('breeds.edit', $breed->id)}}" title="mark as completed"   class="btn social-btn btn-inverse-secondary">
          <i class="text-black fa fa-edit"></i>
        </a></td>
        <td>
          <a href="javascript:void(0)" onclick="destroy('{{$breed->id}}')" title="remove breed"  class="btn social-btn btn-inverse-secondary">
            <i class="fa fa-trash-o text-black"></i>
          </a>
        </td>
      </tr>
      @endforeach
    </tbody>
    <tfoot>
     <tr class="custom-pagination-space-around">
      <td><a href="/breeds/create" class="btn btn-primary btn-fw">Add Breed</a></td>
      <td  colspan="2">{{$breeds->links()}}</td> 
    </tr>
  </tfoot>

</table>
</div>
</div>
</div>
</div>



<script type="text/javascript">

 const destroy = id => {

  if(confirm('Are you sure you want to proceed')) {
   $.ajaxSetup({
    headers: { 'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content }
  })
   $.ajax({
    url: '/breeds/'+id,
    method: 'DELETE',
    data: { 
     id
   },
   success:  res => {
    alert(JSON.stringify(res.success))
    window.location.reload();
  },  
  catch : err => {
   alert(JSON.stringify(err))
 }})
 }

} 


</script>





@endsection