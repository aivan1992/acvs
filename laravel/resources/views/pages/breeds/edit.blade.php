	@extends('template.main')

	@section('body')

	@include('partials.card-header', ['title' => 'Edit Breed'])

	@include('partials.success-error-notification')

	<div class="row">
		<div class="col-lg-12 d-flex justify-center ">
			<div class="col-lg-4 grid-margin stretch-card mx-auto">
				<div class="card">
					<div class="card-body">

						<form class="forms-sample" action="{{route('breeds.update', $model->id)}}" method="POST">
							@method('PUT')
							@csrf
							<div class="form-group">
								<label >Breed Name</label>
								<input value="{{$model->name}}" type="text" name="breed[name]" class="form-control" autofocus>
							</div>

							<div class="mt-5">
								<button type="submit" class="btn btn-primary mr-2">Submit</button>
								<a href="{{route('breeds.index')}}" class="btn btn-light">Cancel</a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	@endsection