@extends('template.main')


@section('body')

@include('partials.card-header', ['title' => 'Create Account'])


@include('partials.success-error-notification')

<div class="row">
  <div class="col-lg-12 d-flex justify-center ">
    <div class="col-lg-6 grid-margin stretch-card offset-lg-3">
      <div class="card">
        <div class="card-body">
          <form class="forms-sample" action="/accounts" method="POST">
            @csrf
            <div class="form-group">
              <label>Name</label>
              <input type="text" class="form-control" name="name">
            </div>
            
          <div class="mt-3">
            <button type="submit" class="btn btn-primary mr-2">Submit</button>
            <a class="btn btn-light" href="{{route('accounts.index')}}">Cancel</a>
          </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection