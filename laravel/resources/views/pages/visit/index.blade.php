  @extends('template.main')

  @section('body')

  @include('partials.card-header', ['title' => 'Visit Request'])

  @include('partials.success-error-notification')


  <div class="col-lg-12 grid-margin stretch-card">
   <div class="card">
    <div class="card-body">
     <table class="table table-hover">
      <thead>
       <tr>
        <th>ID<a href="Javascript:void(0)" title="Click serial to view image"><i class="fa fa-camera text-primary"></i></a></th>
        <th>PET NAME</th>
        <th>SCHEDULE</th>
        <th>USER EMAIL</th>
        <th>MOBILE NO.</th>
        <th>REJECT</th>
        <th>APPROVED</th>
      </tr>
    </thead>
    <tbody>

      @foreach($visits as $visit)

      <tr>
        <td><a href="{{route('visits.show', $visit->id)}}" target="_blank" class="custom-link-text">{{$visit->id}}</a></td>
        <td>
          <textarea readonly class="form-control">{{$visit->catalog()->value('name')}}</textarea>
        </td>
        <td>
          {{$visit->scheduleMutated}}
        </td>
        <td>
          {{$visit->user()->value('email')}}
        </td>
        <td>
          {{$visit->mobile_no}}
        </td>

    <td>

      <a onclick="updateStatus(2, '{{$visit->id}}')" title="reject" class="btn social-btn btn-inverse-secondary">
        <i class="fa fa-trash-o text-black"></i>
      </a>
    </td>
       <td>
          <button type="type" data-toggle="tooltip" title="Approved" onclick="updateStatus(1, '{{$visit->id}}')" class="btn social-btn btn-inverse-secondary">
            <i class="fa fa-check text-black"></i>
    </td>
  </tr>

  @endforeach

</tbody>

</table>

</div>
</div>
</div>



<script>


  const updateStatus = (value, id) =>{

    if(confirm('Are you sure you want to proceed')) {
     $.ajaxSetup({
      headers: { 'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content }
    });

     $.ajax({
      url: "{{url('/visits/status')}}",
      method: 'POST',
      data: {
       status : value,
       id
     },
     success:  res => {
      alert(JSON.stringify(res.success))
      window.location.reload();
    },
    catch : err => {
     alert(JSON.stringify(err))
   }})
   }

 }


</script>

@endsection