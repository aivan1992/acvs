 <!-- custom errors and success notification -->



 @if(session('success'))
 <div class="col-lg-12">
    <div class="alert alert-success">{{session('success')}}</div>
</div>
@endif


@if($message = Session::get('error'))
<div class="col-lg-12">
    <div class="alert alert-danger">{{$message}}</div>

</div>
@endif


@if(count($errors) > 0 ) 
<div class="col-lg-12">
    @foreach($errors->all() as $error)
    <div class="alert alert-danger">{{$error}}</div>
    @endforeach
</div>
@endif


