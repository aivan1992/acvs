<nav class="sidebar sidebar-offcanvas pb-5" id="sidebar">
  <ul class="nav">
    <li class="nav-item nav-profile">
      <a href="#" class="nav-link">
        <div class="profile-image">
          <img class="img-xs rounded-circle" src="{{asset('assets/images/faces-clipart/pic-1.png')}}" alt="profile image">
          <div class="dot-indicator bg-success"></div>
        </div>
        <div class="text-wrapper">
          <p class="profile-name">{{ isset(Auth::user()->name) ? Auth::user()->name : ''}}</p>
          <p class="designation">{{   isset(Auth::user()->name) ? Auth::user()->getRoleNames()->first() : ''}}</p>
        </div>
      </a>
    </li>
    <li class="nav-item nav-category">Main Menu</li>

    <li class="nav-item">
      <a class="nav-link" href="/accounts">
        <i class="menu-icon typcn typcn-document-text"></i>
        <span class="menu-title">Accounts</span>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="/catalogs">
        <i class="menu-icon typcn typcn-document-text"></i>
        <span class="menu-title">Catalog</span>
      </a>
    </li>

  <li class="nav-item">
      <a class="nav-link" href="/breeds">
        <i class="menu-icon typcn typcn-document-text"></i>
        <span class="menu-title">Breed</span>
      </a>
    </li>

     <li class="nav-item">
      <a class="nav-link" href="/categories">
        <i class="menu-icon typcn typcn-document-text"></i>
        <span class="menu-title">Category</span>
      </a>
    </li>
<!--

     <li class="nav-item">
      <a class="nav-link" href="/">
        <i class="menu-icon typcn typcn-document-text"></i>
        <span class="menu-title">Newsletter</span>
      </a>
    </li> -->

<!--      <li class="nav-item">
      <a class="nav-link" href="/">
        <i class="menu-icon typcn typcn-document-text"></i>
        <span class="menu-title">Testimonials</span>
      </a> -->
    </li>

    <!--  <li class="nav-item">
      <a class="nav-link" href="/social-media">
        <i class="menu-icon typcn typcn-document-text"></i>
        <span class="menu-title">Social Media</span>
      </a>
    </li>

     <li class="nav-item">
      <a class="nav-link" href="/contact-details">
        <i class="menu-icon typcn typcn-document-text"></i>
        <span class="menu-title">Contact Details</span>
      </a>
    </li> -->

    <li class="nav-item">
      <a class="nav-link" href="/visits">
        <i class="menu-icon typcn typcn-document-text"></i>
        <span class="menu-title">Visit</span>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="/visits/escalated">
        <i class="menu-icon typcn typcn-document-text"></i>
        <span class="menu-title">Escalated</span>
      </a>
    </li>

  </ul>
</nav>