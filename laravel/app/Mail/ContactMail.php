<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $email, $subject, $message)
    {
        $this->name = $name;
        $this->email = $email;
        $this->subject = $subject;
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

         $info = [
            'name' => $this->name,
            'message' => $this->message,
            'email' => $this->email,
            'subject' => $this->subject
        ];

        return $this->from('admin@careanimalshelter.com')
        ->markdown('mails.contact-mail')
        ->subject($this->subject)
        ->with(['info' => $info]);
    }
}
