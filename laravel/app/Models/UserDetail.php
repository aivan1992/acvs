<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class UserDetail extends Model
{

	// 'first_name' => 'ivan robert',
	// 'last_name' => 'laid',
	// 'zip' => '123123',
	// 'town_city' => 'Dumaguete City',
	// 'province' => 'Negros Oriental',
	// 'address' => 'awdawdawdawawd',
	// 'phone_no' => '0935239239',
	// 'country' => 'philippines',
	// 'email' => 'ivanrobert1992@gmail.com'


	protected $fillable = ['first_name', 'last_name', 'address', 'province', 'zip', 'phone_no','country', 'town_city', 'user_id'];


	public function setFirstNameAttribute($value)
	{
		$this->attributes['first_name'] =  Str::title($value);
	}

	public function setLastNameAttribute($value)
	{
		$this->attributes['last_name'] =  Str::title($value);
	}

	public function setTownCityAttribute($value)
	{
		$this->attributes['town_city'] =  Str::title($value);
	}

	public function setProvinceAttribute($value)
	{
		$this->attributes['province'] =  Str::title($value);
	}

	public function setAddressAttribute($value)
	{
		$this->attributes['address'] =  ucfirst($value);
	}

}
