<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class ContactDetail extends Model
{
	// $table->string('address')->nullable();
	// $table->string('number')->nullable();
	// $table->string('office_sched')->nullable();
	// $table->string('email')->nullable();
	protected $fillable = ['address', 'number','office_sched','email'];
	
	public function setAddressAttribute($value)
	{
		$this->attributes['address'] =  Str::title($value);
	}
}
