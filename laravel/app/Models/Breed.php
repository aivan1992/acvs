<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Breed extends Model
{

	use Sluggable;

	protected $table = 'breeds';
	protected $fillable = ['name', 'slug'];


public function sluggable()
	{
		return [
			'slug' => [
				'source' => 'name'
			]
		];
	}
	   public function setNameAttribute($value)
    {
        $this->attributes['name'] =  Str::title($value);
    }
}
