<?php

namespace App\Models;

use App\Models\Breed;
use App\Models\Category;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Catalog extends Model
{
	use Sluggable;

	protected $table = 'catalogs';
	protected $fillable = ['category_id', 'breed_id', 'name', 'status','slug'];

public function sluggable()
	{
		return [
			'slug' => [
				'source' => 'name'
			]
		];
	}

	public function category()
	{
		return $this->belongsTo(Category::class, 'category_id');
	}

	public function breed()
	{
		return $this->belongsTo(Breed::class, 'breed_id');
	}

	public function images()
	{
		return $this->morphMany(Image::class, 'imageable');
	}

	public function getImagePathAttribute() {
		if(count($this->images)) {
			return  "/". $this->images()->value('directory_name') . '/'. $this->images()->value('file_name');
		}
	}
}
