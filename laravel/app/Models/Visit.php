<?php

namespace App\Models;

use App\Models\Breed;
use App\Models\Catalog;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Visit extends Model
{

	protected $fillable = ['schedule', 'user_id', 'approved', 'mobile_no', 'catalog_id'];

	public function user()
	{
		return $this->belongsTo(User::class, 'user_id');
	}

	public function catalog()
	{
		return $this->belongsTo(Catalog::class, 'catalog_id');
	}

	public function breed()
	{
		return $this->hasOneThrough(
            Breed::class,
            Catalog::class,
            'breed_id',
            'id',
            'catalog_id',
            'id',
        );
	}

	public function getScheduleMutatedAttribute() {
		$result = '';

		if($this->schedule == null) {
			$result = 'not set';
		} else {
			$result =  date('M j, h:i A', strtotime($this->schedule));
		}
		return $result;
	}

	public function getStatusAttribute() {
		$status = 'pending';
		switch($this->approved) {
			case 0;
			$status = 'Pending';
			break;
			case 2;
			$status = 'Rejected';
			break;
			case 1;
			$status = 'Approved';
			break;
			case 3;
			$status = 'Completed';
			break;
		}
		return $status;
	}
}
