<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AccountResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $country = $this->userDetails()->value('country');
       $country = ($country == null || $country == '') ? 'Philippines' : $country; 

        return [
            'first_name' => $this->userDetails()->value('first_name'),
            'last_name' => $this->userDetails()->value('last_name'),
            'zip' => $this->userDetails()->value('zip'),
            'town_city' => $this->userDetails()->value('town_city'),
            'province' => $this->userDetails()->value('province'),
            'address' => $this->userDetails()->value('address'),
            'phone_no' => $this->userDetails()->value('phone_no'),
            'country' => $country,
            'email' => $this->email
        ];
    }
}
