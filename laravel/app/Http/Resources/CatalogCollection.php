<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\Auth;
use Request as Req;

class CatalogCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

          if(Req::has('Authorization') || Req::header('Authorization')) {
            $user = Auth::guard('api')->user();
        }

        return [
            'id' => $this->id,
            'slug' => $this->slug,
            'category' => $this->category()->value('name'),
            'breed' => $this->breed()->value('name'),
            'name' => $this->name,
            'image' =>  url('/') . $this->imagePath,
        ];
    }
}
