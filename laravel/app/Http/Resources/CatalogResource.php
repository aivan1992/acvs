<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CatalogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
         return [
            'id' => $this->id,
            'slug' => $this->slug,
            'category' => $this->category()->value('name'),
            'breed' => $this->breed()->value('name'),
            'name' => $this->name,
            'image' =>  url('/') . $this->imagePath,
        ];
    }
}
