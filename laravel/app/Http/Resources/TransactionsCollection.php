<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class TransactionsCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
             'pet_name' => $this->catalog()->value('name'),
             'breed' => $this->catalog->breed()->value('name'),
             'schedule' => $this->scheduleMutated,
             'status' => $this->status,
        ];
    }
}
