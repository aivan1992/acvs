<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\LaravelImageOptimizer\Facades\ImageOptimizer;
use Spatie\Permission\Models\Role;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // the image will be replaced with an optimized version which should be smaller
      ImageOptimizer::optimize($pathToImage);

// if you use a second parameter the package will not modify the original
      // ImageOptimizer::optimize($pathToImage, $pathToOptimizedImage);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
      DB::beginTransaction();

      try {

        $user = User::create([
         'name' => $request->name,
         'email' => $request->email,
         'password' => Hash::make($request->password),  
       ]);

        /*
        *  CREATE ROLE IF NOT EXIST
        */ 
        if(!Role::whereName($request->roles_radio)->exists()) {

         $role = Role::create(['name' => $request->roles_radio]);

       }

       /*
       *  ASSIGN ROLE
       */
       $user->assignRole($request->roles_radio);

       DB::commit();

       return Redirect::back()->with('success','Data  Saved  Successfully!');

     } catch (\Exception $e) {

       DB::rollback();

       return Redirect::back()->with('error', $e->getMessage());

     }
   }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      
      $user = User::find($id);

      $validatedData = $request->validate([
        'name' => "required|string|max:255",
        'email' => "required|string|email|max:255",
      ]);

     /*
      * DECLARE ARRAY VARIABLE
      */
     $details_array = array(
      'name' => $request->name,
      'email' => $request->email,
    );
        /*
        *
        */

      /*
      * CHECK IF PASSWORD HAS INPUT
      */
      if($request->password != '') {

       $request->validate([
         'password' => "string|min:8|confirmed",
       ]);   

       $password = array('password' => Hash::make($request->password));

       // PUSH PASSWORD INPUT TO ARRAY
       $merged_array = array_merge($details_array, $password);

     }
      /*
      * 
      */

      DB::beginTransaction();

      try {

      /*
      * UPDATE RECORDS
      */
      $user->update($merged_array ?? $details_array);
      /*
      * 
      */

        /*
        *  CREATE ROLE IF NOT EXIST
        */ 
        if(!Role::whereName($request->role)->exists()) {

         $role = Role::create(['name' => $request->role]);

       }
       /*
       *
       */

       /*
       *  UPDATE ROLE
       */
       $user->syncRoles($request->role);
        /*
       *
       */

        DB::commit();

        return Redirect::back()->with('success','Data  Updated  Successfully!');

      } catch (\Exception $e) {

       DB::rollback();

       return Redirect::back()->with('error', $e->getMessage());

     }
   }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
  }
