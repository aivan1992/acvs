<?php

namespace App\Http\Controllers\AdminPanel;

use App\Http\Controllers\Controller;
use App\Models\Breed;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class BreedControllerResource extends Controller
{
  
   private $model; 

   public function  __construct() {

      // $categories = Category::get(['name', 'id']);

      // View::share('categories', $categories);   

      $this->model = new Breed;  

  } 


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $model =  $this->model::paginate(10);

        return view('pages.breeds.index')
        ->with('breeds', $model);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('pages.breeds.create');
  }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $request->validate([
            'name' => "required|string|max:255|unique:breeds",
        ]);

        $this->model::create($request->all());

        return Redirect::back()->with('success','Data Saved Successfully!');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model =  $this->model::find($id);

        return view('pages.breeds.show')
        ->with('model', $model);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

         $model =  $this->model::find($id);

        return view('pages.breeds.edit')
        ->with('model', $model);

 }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
     $model = $this->model::findOrFail($id);

     $model->update(
        $request['breed']);

     return Redirect::back()->with('success','Data Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->model::destroy($id);

        return response()->json(['success'=>'The DELETE has been updated.'], 200);
    }

     public function search(Request $request) {

         $search = $request->search;

         if($request->search != null) {

         /*
         *  SEARCH FOR EXISTING DATA
         */
         $breeds = $this->model::where('name', 'LIKE', '%' . $search . '%')
        // ->orWhere('email', 'LIKE', '%' . $search . '%')
         ->paginate(10);
        /*
         *  
         */

        if (count($breeds) > 0) {

          switch (count($breeds)) {

            case 1:
            $success_message = '1 record has been found';
            break;
            
            default:
            $success_message = count($breeds) . ' records has been found';
            break;

        }


        /*
         * DECLARE SUCCESS SESSION MESSAGE AND REMOVE ERROR SESSION
         */
        $request->session()->forget('error');
        $request->session()->flash('success', $success_message);
        /*
         *
         */

        return view('pages.breeds.index')
        ->with('breeds', $breeds);

    }

}

        /*
         * DECLARE ERROR SESSION MESSAGE AND REMOVE SUCCESS SESSION
         */ 
        $request->session()->forget('success');
        $request->session()->flash('error', 'No specific record found!');
        /*
         *
         */

        return view('pages.breeds.index')
        ->with('breeds', $this->model::paginate(10));

    }



}
