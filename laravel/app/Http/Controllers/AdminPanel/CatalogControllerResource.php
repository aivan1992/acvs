<?php

namespace App\Http\Controllers\AdminPanel;

use App\Http\Controllers\Controller;
// use App\Http\Resources\catalogsCollection;
use App\Models\Category;
use App\Models\Breed;
use App\Models\Image;
use App\Models\Catalog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class CatalogControllerResource extends Controller
{

   private $model;

   public function  __construct() {

      $categories = Category::get(['name', 'id']);
      $breeds = Breed::get(['name', 'id']);

      View::share('categories', $categories);
      View::share('breeds', $breeds);

      $this->model = new Catalog;

  }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

       // $catalogs  = catalogsCollection::collection(Product::all());

        $catalogs =  $this->model::paginate(10);

        return view('pages.catalogs.index')
        ->with('catalogs', $catalogs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


        return view('pages.catalogs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       $request->validate([
        'catalog.name' => 'required',
        'catalog.breed_id' => 'required',
        'catalog.category_id' => 'required',
    ]);

        // SAVE DATE TO GENERATE SLUG
       $catalog =  $this->model::create($request['catalog']);


        /*
        *  PREPARE IMAGE
        *
        */
        $directory = 'files/catalogs/'. $catalog->id;
        $image = $request->file('image');
        $ext = $image->getClientOriginalExtension();

        // PARSE catalog SLUG AND IMG EXTENSION
        $file_name =  $catalog->slug . "." . $ext;

        $imageInfo = new Image([
            'file_name' => $file_name,
            'directory_name' => $directory
        ]);
        /*
        */

        // SAVE IMAGE
        $catalog->images()->save($imageInfo);


        // MOVE IMAGE
        $request->image->move($directory, $file_name);


        return Redirect::back()->withInput($request->all())->with('success','Data Saved Successfully!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
     $catalog =  $this->model::find($id);

     return view('pages.catalogs.show')
     ->with('catalog', $catalog);

 }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $catalog =  $this->model::find($id);
        return view('pages.catalogs.edit')
        ->with('catalog', $catalog);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

     $request->validate([
        'catalog.name' => 'required',
        'catalog.breed_id' => 'required',
        'catalog.category_id' => 'required',
    ]);

     $catalog = $this->model::findOrFail($id);

     if($request->hasFile('image')) {

         $request->validate([
          'image' => 'required|image|mimes:jpeg,png,jpg',
       ]);
         $path = 'files/catalogs/' . $catalog->id;
          /*
          * SEARCH FOR PO FILES
          */
          if(\File::exists($path)){
               \File::deleteDirectory(platformSlashes($path));
         }
         $catalog->images()->delete();

         $image = $request->file('image');
         $ext = $image->getClientOriginalExtension();

        // PARSE catalog SLUG AND IMG EXTENSION
         $file_name =  $catalog->slug . "." . $ext;

         $imageInfo = new Image([
            'file_name' => $file_name,
            'directory_name' => $path
        ]);
        /*
        */

        // SAVE IMAGE
        $catalog->images()->save($imageInfo);

        $request->image->move($path, $file_name);
    }
    $catalog->update(
        $request['catalog']
    );


    return Redirect::back()->with('success','Data Updated Successfully!');
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

     $catalog = $this->model::find($id);

     $path = 'files/catalogs/'. $catalog->slug;
          /*
          * SEARCH FOR PO FILES
          */

          if(\File::exists($path)) {
           \File::deleteDirectory($path);
       }



       $catalog->images()->delete();
       $catalog->delete();

       return response()->json(['success'=>'The DELETE has been updated.'], 200);
   }


   public function search(Request $request) {

       $search = $request->search;

       if($request->search != null && $request->key != null) {

         /*
         *  SEARCH FOR EXISTING DATA
         */
         $catalogs = $this->model::where($request->key, 'LIKE', '%' . $search . '%')
        // ->orWhere('email', 'LIKE', '%' . $search . '%')
         ->paginate(10)->setPath('/');
        /*
         *
         */

        if (count($catalogs) > 0) {

          switch (count($catalogs)) {

            case 1:
            $success_message = '1 record has been found';
            break;

            default:
            $success_message = count($catalogs) . ' records has been found';
            break;

        }


        /*
         * DECLARE SUCCESS SESSION MESSAGE AND REMOVE ERROR SESSION
         */
        $request->session()->forget('error');
        $request->session()->flash('success', $success_message);
        /*
         *
         */

        return view('pages.catalogs.index')
        ->with('catalogs', $catalogs);

    }

}

        /*
         * DECLARE ERROR SESSION MESSAGE AND REMOVE SUCCESS SESSION
         */
        $request->session()->forget('success');
        $request->session()->flash('error', 'No specific record found!');
        /*
         *
         */

        return view('pages.catalogs.index')
        ->with('catalogs', $this->model::paginate(10));

    }


    public function updateStatus(Request $request) {

        $catalog =  $this->model::find($request->id);
        $catalog->status = $request->status;
        $catalog->save();

        return response()->json(['success'=>'The Record has been updated.'], 200);
    }



}
