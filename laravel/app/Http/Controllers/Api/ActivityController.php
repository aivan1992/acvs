<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\AnimalCollection;
use App\Http\Resources\TransactionsCollection;
use App\Mail\ContactMail;
use App\Mail\ContactMailNotification;
use App\Models\Catalog;
use App\Models\Visit;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ActivityController extends Controller
{
	public function sendMessage(Request $request)
	{
		try {
			\Mail::to('ivanrobert1992@gmail.com')
			->send(new ContactMail(
				$request->name,
				$request->email,
				$request->subject,
				$request->message
			));
			\Mail::to($request->email)
			->send(new ContactMailNotification(
				$request->name
			));
			return response()->json(true, Response::HTTP_OK);
		} catch(\Exception $e) {
			return response(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
		}
	}


	public function getTransactions(Request $request)
	{
		try {

				$visits = Visit::whereUserId($request->user()->id)->get();
				$visits = TransactionsCollection::collection($visits);
			return response()->json($visits, Response::HTTP_OK);
		} catch(\Exception $e) {
			return response(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
		}
	}





	public function getCatalogs() {
		try {

			$model = new Catalog;
			$animal = (Url::get('animal') != 'null' && Url::get('animal') != '') ? Url::get('animal') : null;
			switch (true) {
				case ($animal != null):
				$model =  $model::whereHas('animal', function($q) use
					($animal) {
						$q->where('name', $animal);
					})->orderBy('created_at', 'desc')
				->whereStatus(1)
				->paginate(6);
				break;
				default:
				$model = $model->orderBy('created_at', 'desc')
				->paginate(6);
			}
			$modelResource = AnimalCollection::collection($model);
			$response = [
				'pagination' => [
					'total' => $model->total(),
					'per_page' => $model->perPage(),
					'current_page' =>  $model->currentPage(),
					'last_page' =>  $model->lastPage(),
					"page_url" => "products?page=",
					'from' => $model->firstItem()
				],
				'data' =>  $modelResource,
			];
			return response()->json($response, Response::HTTP_OK);
		} catch(\Exception $e) {
			return response(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
		}
	}
}
