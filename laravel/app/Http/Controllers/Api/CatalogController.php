<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CatalogCollection;
use App\Http\Resources\CatalogResource;
use App\Models\Catalog;
use App\Models\Category;
use App\Models\Visit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request as Url;
use Request as Req;
use Symfony\Component\HttpFoundation\Response;

class CatalogController extends Controller
{

 public function  __construct() {

  $this->model = new Catalog;

}



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Req::has('Authorization') || Req::header('Authorization')) {
            $user = Auth::guard('api')->user();
        }

        try {
          $category = (Url::get('category') != 'null' && Url::get('category') != '') ? Url::get('category') : null;

          switch (true) {
            case ($category != null):

            $model =  $this->model->whereHas('category', function($q) use ($category) {
                $q->where('name', $category);
            })->orderBy('created_at', 'desc')
            ->whereStatus(1)
            ->paginate(15);
            break;

            default:
            $model = $this->model->orderBy('created_at', 'desc')->whereHas('category')
            ->whereStatus(1)
            ->paginate(15);
        }

        $modelResource = CatalogCollection::collection($model);
        $response = [
            'pagination' => [
              'total' => $model->total(),
              'per_page' => $model->perPage(),
              'current_page' =>  $model->currentPage(),
              'last_page' =>  $model->lastPage(),
              'from' => $model->firstItem()
          ],
          'data' =>  $modelResource,
      ];
      return response()->json($response, Response::HTTP_OK);
  } catch(\Exception $e) {
     return response(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
 }

}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $catalog =  $this->model::findOrFail($id);
      $catalog = new CatalogResource($catalog);
      return response()->json($catalog, Response::HTTP_OK);
  }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function categories()
    {
       $data = Category::pluck('name');
       return response()->json($data, Response::HTTP_OK);
   }

   public function submitInquire(Request $request)
   {

    try {
       $dateTime = $request->date . ' ' . $request->time . ':00';
       Visit::create([
        'catalog_id' => $request->catalog_id,
        'user_id' => $request->user()->id,
        'mobile_no' => $request->mobile_no,
        'schedule' => $dateTime
    ]);
       return response()->json(true, Response::HTTP_OK);
   } catch(\Exception $e) {
    return response(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
}


}

}
